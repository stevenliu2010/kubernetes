# kubernetes

## 介绍
CentOS7版本部署K8S集群

## 软件架构
kubeadm是官方社区推出的一个用于快速部署kubernetes集群的工具


## 安装教程

### 一、安装要求
- 至少两台服务器，操作系统为CentOS7.X-86_x64
- 硬件配置：2GB或更多RAM，2个CPU或更多CPU，硬盘30GB或更多
- 集群中所有机器之间网络互通
- 可以访问外网，需要拉取镜像(或者提前准备好镜像)
### 二、准备环境
```bash
三台主机
IP:  192.168.101.2     主机名：master    系统： centos 7.6      配置： 2C 2G
IP:  192.168.101.3     主机名：node1     系统： centos 7.6      配置： 2C 2G
IP:  192.168.101.4     主机名：node2     系统： centos 7.6      配置： 2C 2G


更改所有节点的主机名，便于区分（各自执行）：
master节点：hostnamectl --static set-hostname k8s-master
node1节点：hostnamectl --static set-hostname k8s-node1
node2节点：hostnamectl --static set-hostname k8s-node2

bash

# 查看主机名
hostname

```
### 三、安装步骤
#### 1、所以节点关闭防火墙
```bash
systemctl stop firewalld
systemctl disable firewalld
```
#### 2、所有节点关闭selinux
```bash
sed -i "s/SELINUX=enforcing/SELINUX=disabled/g" /etc/selinux/config
setenforce 0
```
#### 3、所有节点关闭swap
```bash
swapoff -a  # 临时关闭

vi /etc/fstab 注释到swap那一行 # 永久关闭

sed -i 's/.*swap.*/#&/g' /etc/fstab
```
#### 4、所有节点添加主机名与IP对应关系（所有机器执行）
```bash
cat >> /etc/hosts << EOF
192.168.101.2 k8s_master
192.168.101.3 k8s_node1
192.168.101.4 k8s_node2
EOF
```
#### 4.1、同步时间（可选）：
```bash
yum install ntpdate -y
ntpdate  ntp.api.bz
```
#### 5、将桥接的IPv4流量传递到iptables的链（所有机器执行）
```bash
cat > /etc/sysctl.d/k8s.conf << EOF
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
sysctl --system
```
#### 6、所有节点安装docker 
```bash
yum -y install wget
wget https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo -O /etc/yum.repos.d/docker-ce.repo
yum -y install docker-ce-18.09.9-3.el7
# 启动docker,并设置docker开机自启
systemctl start docker
systemctl enable docker
# 配置加速，并设置驱动
cat > /etc/docker/daemon.json <<EOF
{
  "registry-mirrors": ["https://6ze43vnb.mirror.aliyuncs.com"],
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF
# 加载daemon并重启docker
systemctl daemon-reload
systemctl restart docker
```
#### 7、所有节点添加阿里云YUM源
```bash
cat > /etc/yum.repos.d/kubernetes.repo << EOF
[kubernetes]
name=Kubernetes
baseurl=https://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=0
repo_gpgcheck=0
gpgkey=https://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg https://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg
EOF
```
#### 8、所有节点安装kubeadm，kubelet和kubectl
- 这里指定了版本号，若需要其他版本的可自行更改
```bash
yum install -y kubelet-1.18.0 kubeadm-1.18.0 kubectl-1.18.0
systemctl enable kubelet
```
#### 9、初始化master节点
- 只在master节点执行
- 由于默认拉取镜像地址k8s.gcr.io国内无法访问，这里指定阿里云镜像仓库地址
- 执行成功以后最后结果会输出
```bash
kubeadm init \
  --apiserver-advertise-address=192.168.101.2 \
  --image-repository registry.aliyuncs.com/google_containers \
  --kubernetes-version v1.18.0 \
  --service-cidr=10.1.0.0/16 \
  --pod-network-cidr=10.244.0.0/16

# 执行成功会输出下面的数据，将下面的数据拷贝到从节点执行(每次都不一样根据自己实际生成的为准，这个是node节点加入集群使用)
kubeadm join 192.168.101.2:6443 --token m42oxn.6a8dk04txtpxd0kt \
    --discovery-token-ca-cert-hash sha256:743178d212dcf0bbf40e1a8d8e3f74604035363b3e085835d7901d57d7263836 


# 在master节点执行
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

# 查看各节点是否连接上主节点
kubectl get nodes
```
#### 10、安装Pod网络插件（CNI）
- 也可使用calico 本次使用flannel网络
```bash
# kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/v0.10.0/Documentation/kube-flannel.yml
# 或
kubectl apply -f kube-flannel.yaml

# 查看pods节点信息（所有节点的状态为：Running，说明正常）
kubectl get pods -n kube-system
```
#### 11、部署nginx测试一下（可选）
- 访问 http://NodeIP:Port
```bash
kubectl create deployment nginx --image=nginx
kubectl expose deployment nginx --port=80 --type=NodePort
kubectl get pod,svc
```
#### 12、部署Dashboard (可用kuboard代替dashboard, [点击查看kuboard安装链接](https://gitee.com/ylp657/kuboard))
- 访问地址：https://NodeIP:30001

创建证书
```bash
mkdir dashboard-certs
cd dashboard-certs/
#创建命名空间
kubectl create namespace kubernetes-dashboard
# 创建key文件
openssl genrsa -out dashboard.key 2048
#证书请求，CN=可修改为实际IP或者域名
openssl req -days 36000 -new -out dashboard.csr -key dashboard.key -subj '/CN=kubernetes-dashboard-certs'
#自签证书
openssl x509 -req -in dashboard.csr -signkey dashboard.key -out dashboard.crt
#创建kubernetes-dashboard-certs对象
kubectl create secret generic kubernetes-dashboard-certs --from-file=dashboard.key --from-file=dashboard.crt -n kubernetes-dashboard
cd ../
```

```bash
kubectl apply -f dashboard.yaml
kubectl get pods -n kubernetes-dashboard
```
- 创建service account并绑定默认cluster-admin管理员集群角色
- 使用输出的token登录Dashboard
- 部分浏览器可能无法访问，经测试firefox可以
```bash
kubectl create serviceaccount dashboard-admin -n kubernetes-dashboard
kubectl create clusterrolebinding dashboard-admin --clusterrole=cluster-admin --serviceaccount=kubernetes-dashboard:dashboard-admin
# 获取登录token
kubectl describe secrets -n kubernetes-dashboard $(kubectl -n kubernetes-dashboard get secret | awk '/dashboard-admin/{print $1}')
```

(完)

#### 13、部署Metrics-Server服务
##### 13.1、下载并解压Metrics-Server
```bash
wget https://github.com/kubernetes-sigs/metrics-server/archive/v0.3.6.tar.gz
tar -zxvf v0.3.6.tar.gz 
```
##### 13.2、修改Metrics-Server配置文件
```bash
cd metrics-server-0.3.6/deploy/1.8+/
vi metrics-server-deployment.yaml
```
- vim metrics-server-deployment.yaml文件原有的内容如下所示。
```yaml
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: metrics-server
  namespace: kube-system
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: metrics-server
  namespace: kube-system
  labels:
    k8s-app: metrics-server
spec:
  selector:
    matchLabels:
      k8s-app: metrics-server
  template:
    metadata:
      name: metrics-server
      labels:
        k8s-app: metrics-server
    spec:
      serviceAccountName: metrics-server
      volumes:
      # mount in tmp so we can safely use from-scratch images and/or read-only containers
      - name: tmp-dir
        emptyDir: {}
      containers:
      - name: metrics-server
        image: k8s.gcr.io/metrics-server-amd64:v0.3.6
        imagePullPolicy: Always
        volumeMounts:
        - name: tmp-dir
          mountPath: /tmp
```
- 修改后的文件内容如下所示。
```yaml
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: metrics-server
  namespace: kube-system
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: metrics-server
  namespace: kube-system
  labels:
    k8s-app: metrics-server
spec:
  selector:
    matchLabels:
      k8s-app: metrics-server
  template:
    metadata:
      name: metrics-server
      labels:
        k8s-app: metrics-server
    spec:
      serviceAccountName: metrics-server
      volumes:
      # mount in tmp so we can safely use from-scratch images and/or read-only containers
      - name: tmp-dir
        emptyDir: {}
      containers:
      - name: metrics-server
        # 修改image 和 imagePullPolicy
        image: mirrorgooglecontainers/metrics-server-amd64:v0.3.6
        imagePullPolicy: IfNotPresent
        # 新增command配置
        command:
        - /metrics-server
        - --kubelet-insecure-tls
        - --kubelet-preferred-address-types=InternalDNS,InternalIP,ExternalDNS,ExternalIP,Hostname
        volumeMounts:
        - name: tmp-dir
          mountPath: /tmp
        # 新增resources配置
        resources:
          limits:
            cpu: 300m
            memory: 200Mi
          requests:
            cpu: 200m
            memory: 100Mi
```
- 修改完metrics-server-deployment.yaml文件后保存退出。
##### 13.3、安装Metrics-Server
- 执行如下命令安装Metrics-Server。
```bash
kubectl apply -f metrics-server-0.3.6/deploy/1.8+/
```
##### 13.4、查看node信息
```bash
[root@binghe101 ~]# kubectl top node
NAME        CPU(cores)   CPU%   MEMORY(bytes)   MEMORY%   
binghe101   141m         7%     1113Mi          65%       
binghe102   62m          3%     549Mi           32% 
binghe103   100m         5%     832Mi           48%
```
```bash
kubectl top pod --all-namespaces
```
##### 13.5、 注意事项
- 下载Metrics-Server安装文件之后，一定要修改metrics-server-deployment.yaml文件，在使用kubectl top node命令查看node信息时，会报如下错误。
- 部署成功以后等一两分钟在使用kubectl top node命令查看
```bash
error: metrics not available yet
```

#### 安装ingress-nginx（可选）
```bash
kubectl create -f ingress-deploy-0.35.0.yaml

# 检查服务
kubectl get pod,svc,job -n ingress-nginx
```

#### 使用说明

1.  如果网速过慢可以使用提供的文件
```bash
dashboard.yaml
kube-flannel.yaml
calico-3.13.1.yaml
v0.3.6.tar.gz
```

2. 
```bash
# 查看pod
kubectl get pod -n kube-system

# 遇到异常状态0/1的pod长时间启动不了可删除它等待集群创建新的pod资源
kubectl delete pod <pod name> -n kube-system

#  查看某个节点失败原因
kubectl describe pod <pod name> -n kube-system
```
3. 从节点重新注册
```bash
kubeadm reset

# 查看master的kubeadm join命令
kubeadm token create --print-join-command
```

4. 报错：failed to set bridge addr: "cni0" already has an IP address different from 10.244.2.1/24
```bash
# 查看是哪个从节点，在从节点执行
sudo yum install net-tools
ifconfig cni0 down    
ip link delete cni0
```
